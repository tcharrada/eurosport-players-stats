import React from 'react';
import './App.css';

import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import PlayerStats from './components/players';

// -------------------------------------------------------------------------
// Using ApolloClient as GraphQL Client
// Initializing ApolloClient Instance
// -------------------------------------------------------------------------

const client = new ApolloClient({
  uri: 'https://kf9p4bkih6.execute-api.eu-west-1.amazonaws.com/dev/',
  cache: new InMemoryCache()
});

// -------------------------------------------------------------------------
// We connect ApolloClient to React using ApolloProvider component 
// and we need to put it here above any component that might need 
// to access GraphQL data
// -------------------------------------------------------------------------

function App() {
  return (
    <ApolloProvider client={client}>
      <div className="App">
        <header className="App-header">
          <img src="./Eurosport_logo.png" className="custom-logo" alt="logo" />
        </header>
        <body className="App-body">
          <PlayerStats />
        </body>
      </div>
    </ApolloProvider>
    
  );
}

export default App;
