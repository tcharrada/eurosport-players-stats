import React from 'react';
import { Card, CardContent, Avatar, Typography } from '@material-ui/core';


type CardProps = {
  lastname: string,
  picture: Picture,
  stats: Stats
}
type Stats = {
  rank: number,
  points: number,
  weight: number,
  height: number,
  age: number
}
type Picture = {
  url: string
}

const PlayerCard = ({lastname, picture, stats}: CardProps) => (
  <Card>
    <CardContent>
      <Avatar alt="player" src={picture.url} />
      <Typography>Name: {lastname}</Typography>
      <Typography>Rank: {stats.rank}</Typography>
      <Typography>Points: {stats.points}</Typography>
      <Typography>Height: {stats.height / 100} M</Typography>
      <Typography>Weight: {stats.weight / 1000} kg</Typography>
    </CardContent>      
  </Card>
)

export default PlayerCard;