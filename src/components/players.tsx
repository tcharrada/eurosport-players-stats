import { useQuery, gql } from '@apollo/client';
import { makeStyles } from '@material-ui/core/styles';
import PlayerCard from './playerCard';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(1),
      width: theme.spacing(36),
      height: theme.spacing(26),
    },
  }
}));

type Players = {
  firstname: string,
  lastname: string,
  picture: Picture,
  stats: Stats
}
type Stats = {
  rank: number,
  points: number,
  weight: number,
  height: number,
  age: number
}
type Picture = {
  url: string
}

const PLAYERS_STATS = gql`
  query GetPlayersStats {
    headToHead { 
      firstname, lastname, picture {
        url}, stats {
        rank,
        points,
        weight,
        height,
        age
      }
    }
  }
`;

// -------------------------------------------------------------------------
// useQuery is a react hook that use the Hooks API
// to share GraphQL data with your UI
// -------------------------------------------------------------------------

function PlayerStats() {
  const classes = useStyles();
  const { loading, error, data } = useQuery(PLAYERS_STATS);
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  return data.headToHead.map(({firstname, lastname, picture, stats}: Players) => (
    <div key={firstname} className={classes.root}> 
      <PlayerCard lastname={lastname} picture={picture} stats={stats} />
    </div>
  ))
}

export default PlayerStats;